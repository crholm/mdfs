package mdfs.namenode.repositories;
/**
 * A enum that represtents the type of data, a file or a dir
 * @author Rasmus Holm
 *
 */
public enum DataTypeEnum {
	FILE, DIR
}
